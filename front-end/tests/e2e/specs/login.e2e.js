// For authoring Nightwatch tests, see
// https://nightwatchjs.org/guide

module.exports = {
  // Each method is a step in nightwatch/selenium  - here i only have one step
  'login test': function (browser) {
    browser
      .url(process.env.VUE_DEV_SERVER_URL + 'login')
      .waitForElementVisible('#app',5000)
      .assert.containsText('h1','TaskAgile')
      .end()

  }


}
