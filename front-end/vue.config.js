module.exports = {
  devServer: {
    port: 3000,

    // With this config all the API requests (but not page requests) will be bridged
    // to the backend. http-proxy-middleware also supports other ways of matching URIs (guide on their Github repo)
    proxy: {
      '/api/*': {
        target: 'http://localhost:8080'
      }
    }
  },

  // Import Bootstraps compiled CSS into my application
  // One of many approaches to import bootstrap using webpack
  // this approach is to create a new entry in webpack to group all of the third party styles into a single .css file
  configureWebpack: {
    entry: {
      app: './src/main.js',
      style: [
        'bootstrap/dist/css/bootstrap.min.css'
      ]
    }
  }
}
